import matplotlib.pyplot as plt
import csv
import glob
from math import sqrt
from math import pow


import csv
import glob


teams = dict()

class Team: ## klasa przechowująca dane odnośnie drużyny piłkarskiej
    def __init__(self, total_games_played=0, games_played_home=0, games_played_away=0,
                 total_goals_conceded=0, goals_conceded_home=0, goals_conceded_away=0,
                 total_goals_scored=0, goals_scored_home=0, goals_scored_away=0,
                 team_defeated_home=[], team_defeated_away=[], draw_home=[], draw_away=[],
                 team_lost_home=[], team_lost_away=[], total_matches_won=0, total_matches_lost=0,
                 total_matches_draw=0, games_won_home=0, games_won_away=0, games_lost_home=0,
                 games_lost_away=0, games_draw_home=0, games_draw_away=0):

        self.total_games_played = total_games_played
        self.games_played_home = games_played_home
        self.games_played_away = games_played_away
        self.total_goals_conceded = total_goals_conceded
        self.goals_conceded_home = goals_conceded_home
        self.goals_conceded_away = goals_conceded_away
        self.total_goals_scored = total_goals_scored
        self.goals_scored_home = goals_scored_home
        self.goals_scored_away = goals_scored_away
        self.team_defeated_home = team_defeated_home
        self.team_defeated_away = team_defeated_away
        self.draw_home = draw_home
        self.draw_away = draw_away
        self.team_lost_home = team_lost_home
        self.team_lost_away = team_lost_away
        self.total_matches_won = total_matches_won
        self.total_matches_lost = total_matches_lost
        self.total_matches_draw = total_matches_draw
        self.games_won_home = games_won_home
        self.games_won_away = games_won_away
        self.games_lost_home = games_lost_home
        self.games_lost_away = games_lost_away
        self.games_draw_home = games_draw_home
        self.games_draw_away = games_draw_away

    def frequency_win_t(self) -> float:
        if self.total_games_played != 0:
            return (self.total_matches_won / self.total_games_played)
        else:
            return 0

    def frequency_win_h(self) -> float:
        if self.games_played_home != 0:
            return (self.games_won_home / self.games_played_home)
        else:
            return 0

    def frequency_win_a(self) -> float:
        if self.games_played_away != 0:
            return (self.games_won_away / self.games_played_away)
        else:
            return 0

    def frequency_draw_t(self) -> float:
        if self.total_games_played != 0:
            return (self.total_matches_draw / self.total_games_played)
        else:
            return 0

    def frequency_draw_h(self) -> float:
        if self.games_played_home != 0:
            return (self.games_draw_home / self.games_played_home)
        else:
            return 0

    def frequency_draw_a(self) -> float:
        if self.games_played_away != 0:
            return (self.games_draw_away / self.games_played_away)
        else:
            return 0

    def avg_goals_conceded_t(self) -> float:
        if self.total_games_played != 0:
            return (self.total_goals_conceded / self.total_games_played)
        else:
            return 0

    def avg_goals_conceded_h(self) -> float:
        if self.games_played_home != 0:
            return (self.goals_conceded_home / self.games_played_home)
        else:
            return 0

    def avg_goals_conceded_a(self) -> float:
        if self.games_played_away != 0:
            return (self.goals_conceded_away / self.games_played_away)
        else:
            return 0

    def avg_goals_scored_t(self) -> float:
        if self.total_games_played != 0:
            return (self.total_goals_scored / self.total_games_played)
        else:
            return 0

    def avg_goals_scored_h(self) -> float:
        if self.games_played_home != 0:
            return (self.goals_scored_home / self.games_played_home)
        else:
            return 0

    def avg_goals_scored_a(self) -> float:
        if self.games_played_away != 0:
            return (self.goals_scored_away / self.games_played_away)
        else:
            return 0

# path = "\proj\*.csv"  ## ścieżka do folderu zawierającego 5 plików ze statystykami
path = "statics\*.csv"  ## ścieżka do folderu zawierającego 5 plików ze statystykami

files = glob.glob(path)
for name in files:
    with open(name) as csv_files:
        plots = csv.reader(csv_files, delimiter = ',')  ##obsługa plików .csv za pomocą biblioteki standardowej csv
        next(plots)
        for row in plots:
            hometeam = teams[row[2]] if row[2] in teams else Team()
            awayteam = teams[row[3]] if row[3] in teams else Team()

            if row[4] == '' or row[5] == '':
                row[4] = 0
                row[5] = 0

            hometeam.total_games_played += 1
            awayteam.total_games_played += 1
            hometeam.total_goals_scored += int(row[4])    ##próba nadania odpowiednich wartości dla kolejnych pól klasy
            hometeam.goals_scored_home += int(row[4])
            awayteam.total_goals_conceded += int(row[4])
            awayteam.goals_conceded_away += int(row[4])
            hometeam.total_goals_conceded += int(row[5])
            hometeam.goals_conceded_home += int(row[5])
            awayteam.goals_scored_away += int(row[5])
            awayteam.goals_scored_away += int(row[5])


            if row[6] == 'H':  ## określenie H = wygrana drużyny gospodarzy D = remis A = wygrana drużyny przyjezdnej
                hometeam.total_matches_won += 1
                awayteam.total_matches_lost += 1
                hometeam.games_won_home += 1
                awayteam.games_lost_away += 1

            if row[6] == 'D':
                hometeam.total_matches_draw += 1
                awayteam.total_matches_draw += 1
                hometeam.games_draw_home += 1
                awayteam.games_draw_away += 1

            if row[6] == 'A':
                hometeam.total_matches_lost += 1
                awayteam.total_matches_won += 1
                hometeam.games_lost_home += 1
                awayteam.games_won_home += 1

            teams[row[2]] = hometeam
            teams[row[3]] = awayteam



def score(team: Team):
    game_points = 3*(team.frequency_win_a()*1.5 + team.frequency_win_h()) + team.frequency_draw_t()
    goals=1.2*team.avg_goals_scored_a()+team.avg_goals_scored_h()-0.8*team.avg_goals_conceded_h()-0.5*team.avg_goals_conceded_a()
    return (game_points+goals)*2


def sim_pearson(prefs, p1, p2):
    si = {}
    for item in prefs[p1]:
        if item in prefs[p2]: si[item] = 1
    n = len(si)
    if n==0: return 0

    sum1 = sum(prefs[p1][it] for it in si)
    sum2 = sum(prefs[p2][it] for it in si)
    sum1sq = sum(pow(prefs[p1][it], 2) for it in si)
    sum2sq = sum(pow(prefs[p2][it], 2) for it in si)
    psum = sum(prefs[p1][it] * prefs[p2][it] for it in si)

    num = psum - (sum1 * sum2 / n)
    den = sqrt((sum1sq-pow(sum1, 2)/n)*(sum2sq-pow(sum2, 2)/n))
    if den == 0: return 0

    r = num / den
    return r


def who_would_win(team1, team2):
    pearson=sim_pearson(teams_dict, team1, team2)
    if score(team1)>score(team2):
        print('wygra drużyna nr {} z prawdopodobienstwem {}'.format(2, (1.5-pearson)))
    else:
        print('wygra drużyna nr {} z prawdopodobienstwem {}'.format(2, (1.5-pearson)))

arsenal = teams['Arsenal']
leicester = teams['Leicester']
brighton = teams['Brighton']
man_united = teams['Man United']
man_city = teams['Man City']
chelsea = teams['Chelsea']
burnley = teams['Burnley']
crystal_palace = teams['Crystal Palace']
everton = teams['Everton']
stoke = teams['Stoke']
southampton = teams['Southampton']
swansea = teams['Swansea']
watford = teams['Watford']
liverpool = teams['Liverpool']
west_ham = teams['West Ham']
west_brom = teams['West Brom']
newcastle = teams['Newcastle']
tottenham = teams['Tottenham']
bournemouth = teams['Bournemouth']
huddersfield = teams['Huddersfield']
hull = teams['Hull']
middlesbrough = teams['Middlesbrough']
sunderland = teams['Sunderland']

all_teams = [arsenal, leicester, brighton, man_city, man_united, chelsea, burnley, crystal_palace, everton, stoke,
             southampton, swansea, watford, liverpool, west_brom, west_ham, newcastle, tottenham, bournemouth,
             huddersfield, hull, middlesbrough, sunderland]

team_names = ['arsenal', 'leicester', 'brighton', 'man_city', 'man_united', 'chelsea', 'burnley', 'crystal_palace',
              'everton', 'stoke', 'southampton', 'swansea', 'watford', 'liverpool', 'west_brom', 'west_ham',
              'newcastle', 'tottenham', 'bournemouth', 'huddersfield', 'hull', 'middlesbrough', 'sunderland']

all_goals_scored = [arsenal.total_goals_scored, leicester.total_goals_scored, brighton.total_goals_scored,
             man_city.total_goals_scored, man_united.total_goals_scored, chelsea.total_goals_scored,
             burnley.total_goals_scored, crystal_palace.total_goals_scored, everton.total_goals_scored,
             stoke.total_goals_scored, southampton.total_goals_scored, swansea.total_goals_scored,
             watford.total_goals_scored, liverpool.total_goals_scored, west_brom.total_goals_scored,
             west_ham.total_goals_scored, newcastle.total_goals_scored, tottenham.total_goals_scored,
             bournemouth.total_goals_scored, huddersfield.total_goals_scored, hull.total_goals_scored,
             middlesbrough.total_goals_scored, sunderland.total_goals_scored]

all_goals_conceded = [arsenal.total_goals_conceded, leicester.total_goals_conceded, brighton.total_goals_conceded,
             man_city.total_goals_conceded, man_united.total_goals_conceded, chelsea.total_goals_conceded,
             burnley.total_goals_conceded, crystal_palace.total_goals_conceded, everton.total_goals_conceded,
             stoke.total_goals_conceded, southampton.total_goals_conceded, swansea.total_goals_conceded,
             watford.total_goals_conceded, liverpool.total_goals_conceded, west_brom.total_goals_conceded,
             west_ham.total_goals_conceded, newcastle.total_goals_conceded, tottenham.total_goals_conceded,
             bournemouth.total_goals_conceded, huddersfield.total_goals_conceded, hull.total_goals_conceded,
             middlesbrough.total_goals_conceded, sunderland.total_goals_conceded]

labels = ['arsenal', 'leicester', 'brighton', 'man_city', 'man_united', 'chelsea', 'burnley', 'crystal_palace',
              'everton', 'stoke', 'southampton', 'swansea', 'watford', 'liverpool', 'west_brom', 'west_ham',
              'newcastle', 'tottenham', 'bournemouth', 'huddersfield', 'hull', 'middlesbrough', 'sunderland']

other_value_scored = int((leicester.total_goals_scored + brighton.total_goals_scored +
                    burnley.total_goals_scored +
                           crystal_palace.total_goals_scored + everton.total_goals_scored + stoke.total_goals_scored +
                           southampton.total_goals_scored + swansea.total_goals_scored + watford.total_goals_scored +
                           west_brom.total_goals_scored + west_ham.total_goals_scored + newcastle.total_goals_scored +
                           bournemouth.total_goals_scored + huddersfield.total_goals_scored +
                           hull.total_goals_scored + middlesbrough.total_goals_scored + sunderland.total_goals_scored))

label = ['ManU', 'Liverpool', 'Chelsea', 'Arsenal', 'Tottenham', 'ManC', 'Others']

values_scored = [man_united.total_goals_scored, liverpool.total_goals_scored, chelsea.total_goals_scored,
                          arsenal.total_goals_scored, tottenham.total_goals_scored, man_city.total_goals_scored,
                          other_value_scored]

cols = ['b','g','r','y','c','m','beige']


other_value_conceded = int((leicester.total_goals_conceded + brighton.total_goals_conceded +
                               burnley.total_goals_conceded + crystal_palace.total_goals_conceded +
                               everton.total_goals_conceded + stoke.total_goals_conceded + southampton.total_goals_conceded +
                               swansea.total_goals_conceded + watford.total_goals_conceded +
                               west_brom.total_goals_conceded + west_ham.total_goals_conceded +
                               newcastle.total_goals_conceded + bournemouth.total_goals_conceded +
                               huddersfield.total_goals_conceded + hull.total_goals_conceded +
                               middlesbrough.total_goals_conceded + sunderland.total_goals_conceded))


values_conceded = [man_united.total_goals_conceded, liverpool.total_goals_conceded, chelsea.total_goals_conceded,
                          arsenal.total_goals_conceded, tottenham.total_goals_conceded, man_city.total_goals_conceded,
                          other_value_conceded]


"""
teams_dict = {}

for team in all_teams:
    teams_dict[team] = {'winning': team.frequency_win_t(), 'goals': team.avg_goals_scored_t(),
                        'score': score(team), 'pearson': 0}
for team1 in all_teams:
    for team2 in all_teams:
        teams_dict[team1]['pearson'] = (teams_dict[team1]['pearson'] + sim_pearson(teams_dict, team1, team2))

for i, team in zip(range(0, 22), all_teams):
        print(team_names[i], ': punkty pearsona: {} pkt,\t score: {}'.format(teams_dict[team]['pearson'], teams_dict[team]['score']))
"""
def MainMenu():
    print("Press 1 to display bar charts of goals scored")
    print("Press 2 to display bar charts of goals conceded")
    print("Press 3 to display score point and pearson")
    print("Press 4 to display pie plot showing impact on total amount of goals scored")
    print("Press 5 to display pie plot showing impact on total amount of goals conceded")
    print("Press 6 to check which team is more likely to win a match")
    print("Press 7 to display bookie rate")
    print("Press 8 to display total statistics of particular team")
    print("Press 9 to quit")
    while True:
        try:
            selection = int(input("Enter choice: "))

            if selection == 1:

                plt.bar(team_names, all_goals_scored, width=0.7, color='g')
                plt.ylabel("Amount of goals")
                plt.title("Goals scored in total")
                plt.xticks(team_names, labels, rotation='vertical')
                plt.margins(0.2)
                plt.subplots_adjust(bottom=0.15)
                plt.legend()
                plt.show()
                
                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 2:


                plt.bar(team_names, all_goals_conceded, width = 0.7, color='r')
                plt.ylabel("Amount of goals")
                plt.title("Goals conceded in total")
                plt.xticks(team_names, labels, rotation = 'vertical')
                plt.margins(0.2)
                plt.subplots_adjust(bottom = 0.15)
                plt.legend()
                plt.show()

                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 3:
                teams_dict = {}

                for team in all_teams:
                    teams_dict[team] = {'winning': team.frequency_win_t(), 'goals': team.avg_goals_scored_t(),
                                        'score': score(team), 'pearson': 0}
                for team1 in all_teams:
                    for team2 in all_teams:
                        teams_dict[team1]['pearson'] = (teams_dict[team1]['pearson'] + sim_pearson(teams_dict, team1, team2))

                for i, team in zip(range(0, 22), all_teams):
                    print(team_names[i], ': punkty pearsona: {0:.2f} pkt,\t score: {0:.2f}'.format(teams_dict[team]['pearson'],
                                                                                         teams_dict[team]['score']))
                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 4:

                plt.pie(values_scored,
                        labels=label,
                        colors=cols,
                        startangle=90,
                        shadow=True,
                        autopct='%1.1f%%')
                plt.title("Impact on total amount of goals scored throughout seasons")
                plt.show()

                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 5:

                plt.pie(values_conceded,
                        labels=label,
                        colors=cols,
                        startangle=90,
                        shadow=True,
                        autopct='%1.1f%%')
                plt.title("Impact on total amount of goals conceded throughout seasons")
                plt.show()

                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 6:
                

                choice1 = str(input("Wprowadź nazwę gospodarza: "))
                choice2 = str(input("Wprowadź nazwę drużyny przyjezdnej: "))

                pref1 = teams[choice1]
                pref2 = teams[choice2]

                who_would_win(pref1, pref2)

                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 7:

                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 8:

                print("Here's a list of teams that you can choose from :")
                for team in team_names:
                    print(str(team))

                choice = str(input("Enter name of a team to show particular stistics: "))

                selected_team = teams[choice]



                print("Amount of goals scored in total {} :{}".format(choice, selected_team.goals_scored_home+selected_team.goals_scored_away))
                print("Amount od goals conceded in total: {} :{}".format(choice, selected_team.goals_conceded_away+selected_team.goals_conceded_home))
                print("Amount of goals scored home {} :{}".format(choice, selected_team.goals_scored_home))
                print("Amount of goals conceded home {} :{}".format(choice, selected_team.goals_conceded_home))
                print("Amount of goals scored away {} :{}".format(choice, selected_team.goals_scored_away))
                print("Amount of goals conceded away {} :{}".format(choice, selected_team.goals_conceded_away))
                print("Frequency of winning in total = {} :{}".format(choice, round(selected_team.frequency_win_t(),2)))


                anykey = input("Enter something to return to main menu:")
                MainMenu()

            elif selection == 9:

                exit()

            else:
                print("Invalid value given, press proper one(1-9)")
                MainMenu()
        except ValueError:
            print("Invalid choice. You were supposed to press (1-9), try again :)")

MainMenu()

